# libKIM101
libKIM101 is a library for accessing the KIM101 device by Thorlabs on Linux. Its usage depends on D2XX driver which can be downloaded from the [ftdi drivers page](https://www.ftdichip.com/Drivers/D2XX.htm). The current implementation is only able to move the motor, with the ability to modify the jog parameters will arrive in a future update. Communication with the device takes place through the exchange of messages defined in the [APT Commonications Protocol](https://www.thorlabs.com/Software/Motion%20Control/APT_Communications_Protocol.pdf). In the code, the messages are designed as a custom object, MSG. In the future, all the messages defined in the protocol will be available. 

Examples on the operation of the device can be found in the apps/ directory, where currently three apps are
available:
- device_scan: shows the device information for any device connected.
- move_channel [ch] [pos]: Move the specified channel to the selected position. 
- zero_all_positions: return all channels to 0. 
- set_position_counter [ch] [pos]: sets the current position for the channel at the requested position. The common use scenario is resetting a position to 0. 

To use the library, a kim101 object has to be created. The serial number of the device which will be controlled can be passed as an argument. The argument mandatory if more than one devices are connected, else the device which is first found will be selected. Once the object has been created, the init() function must be called, which will initialize the device and will start the automatic status update messages.  Similarly, at the end of the program, finalize() must be called. This function first terminates the automatic status update messages and then close the connection to the device. Apart from the aforementioned functions, the other functions available now are the following:
* hw_req_info();
* hw_start_updatemsgs()
* hw_stop_updatemsgs()
* enable_channel(uint8_t ch)
* pzmot_move_absolute(uint8_t channel, long long int pos)
* get_positions()
* zero_all()