#include <kim101/kim101.h>

namespace LIBKIM101 {

    FT_STATUS device_scan(bool verbose, std::vector<unsigned int>* serialNumbers)
    {
	FT_STATUS ftStatus;
	DWORD flags;
	DWORD id;
	DWORD type;
	DWORD locId;
	uint32_t numDevs;
	char serialNumber[16];
	char description[64];

	//Set the VID:PID combination, necessary since the driver does not include this 
	//combination by default. To find it, run lsusb and find
	//"Future Technology Devices International"
	ftStatus = FT_SetVIDPID(VID,PID);
	if(ftStatus != FT_OK)
	    return ftStatus;

	ftStatus = FT_CreateDeviceInfoList(&numDevs);
	if (ftStatus != FT_OK)
	    return ftStatus;

	if (numDevs <= 0)
	    return FT_DEVICE_NOT_FOUND;

	for (int dev = 0; dev < numDevs; dev++) {
	    ftStatus = FT_GetDeviceInfoDetail(dev, &flags, &type, &id,
					      &locId, serialNumber,
					      description,
					      NULL);

	    if (serialNumbers != nullptr)
		(*serialNumbers).push_back(atoi(serialNumber));

	    if(ftStatus != FT_OK) {
		std::cout << "Failed" << std::endl;
		return ftStatus;
	    }

	    if(verbose) {
		std::cout << "Dev " << dev << ":" << std::endl;
		std::cout << std::hex << "\tFlags = 0x" << flags << std::dec << std::endl;
		std::cout << std::hex << "\tType = 0x" << type << std::dec << std::endl;
		std::cout << std::hex << "\tID = 0x" << id << std::dec << std::endl;
		std::cout << std::hex << "\tLocID = 0x" << locId << std::dec << std::endl;
		std::cout << "\tSerial Number = " << serialNumber << std::endl;
		std::cout << "\tDespription = " << description << std::endl;
	    }
	}

	return FT_OK;

    }

    KIM101::KIM101(const unsigned int SN) : serialNumber(std::to_string(SN))
    {
	ftStatus = FT_SetVIDPID(VID,PID);
    }

    MSG::MSG(std::initializer_list<uint8_t> msg) : data(msg) {}

    MSG::MSG(std::vector<uint8_t> msg, const int msg_size)
    {
	data.assign(msg.begin(), msg.begin() + msg_size);
    }

    uint MSG::size()
    {
	return data.size();
    }

    std::ostream& operator<<(std::ostream& os, const class MSG &msg)
    {
	for (const auto &i: msg.data)
	    os << std::hex << std::setfill('0') << std::setw(2)
	       << static_cast<int>(i) << ":" ;
	os << std::dec;

	return os;
    }

    bool MSG::operator==(const MSG& other ) const
    {
	return std::equal(data.begin(), data.begin() + 4, other.data.begin());
    }


//Initialization function which sets the parameters for the operation of the
//device. Mainly copied and pasted from the documentation
    FT_STATUS KIM101::init()
    {
	std::vector<unsigned int> serialNumbers;
	if (serialNumber.empty()) {
	    device_scan(false, &serialNumbers);
	    if (serialNumbers.size() == 1) {
		serialNumber = serialNumbers[0];
	    }
	    else if (serialNumbers.size()> 1) {
		std::cerr << "More than one devices found and no serial number provided. Aborting" << std::endl;
		return 1;
	    }
	}

	ftStatus = FT_OpenEx(static_cast<PVOID>(&serialNumber[0]),FT_OPEN_BY_SERIAL_NUMBER,&ftHandle); 
	if (ftStatus != FT_OK) {
	    std::cout << "Device with #" << serialNumber << " not found. Aborting." << std::endl;
	    return ftStatus;
	}

	EventMask = FT_EVENT_RXCHAR | FT_EVENT_MODEM_STATUS | FT_EVENT_LINE_STATUS;

	//Default values taken from the documentation
	ftStatus  = FT_SetBaudRate(ftHandle, 115200);
	ftStatus |= FT_SetDataCharacteristics(ftHandle, FT_BITS_8,
					      FT_STOP_BITS_1, FT_PARITY_NONE);
	usleep(500);
	ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); 
	usleep(500); 
	ftStatus |= FT_ResetDevice(ftHandle);
	ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0, 0); 
	ftStatus |= FT_SetRts(ftHandle);
	ftStatus |= FT_SetTimeouts(ftHandle, 5000, 5000);
	ftStatus |= FT_SetEventNotification(ftHandle, EventMask, (PVOID)&eh);
	if(ftStatus != FT_OK) {
	    return ftStatus;
	}

	//Enable the polling and send the message to activate update
	//messages
	hw_start_updatemsgs();
	polling_active = 1;
	polling = std::thread(&KIM101::msg_polling, this, &polling_active);
	get_positions(false);

	return ftStatus;
    }


//Function which stops the polling and closes the device. 
    FT_STATUS KIM101::finalize()
    {
	polling_active = 0;
	hw_stop_updatemsgs();
	polling.join();
	FT_Close(ftHandle);
	return ftStatus;
    }


//Write to the device
    FT_STATUS KIM101::write(MSG msg)
    {
	DWORD written = 0;
	ftStatus = FT_Write(ftHandle, &msg[0], msg.size(), &written);
	return ftStatus;
    }


//Read from the device. 
    FT_STATUS KIM101::read(int expectedBytes)
    {
	DWORD rxBytes, txBytes, eventDWord;
	ftStatus = FT_GetStatus(ftHandle, &rxBytes, &txBytes, &eventDWord);

	if (eventDWord == 1) {
	    BYTE rxBuffer[rxBytes];
	    DWORD bytesReceived;
	    ftStatus = FT_Read(ftHandle, rxBuffer, rxBytes, &bytesReceived);
	    processBytestream(rxBuffer, rxBytes);
	}
	return ftStatus;
    }


    void KIM101::processBytestream(BYTE buffer[], unsigned int nbytes)
    {
	std::vector<uint8_t> tmpVec(&buffer[0], &buffer[nbytes]);
	polling_buffer.insert(polling_buffer.end(), tmpVec.begin(), tmpVec.end());
	// std::cout << "Initial size: " << polling_buffer.size() << std::endl;

	while(1) {
	    //Examine if we have enough bytes, 
	    if (polling_buffer.size() < HEADER_SIZE)
		break;

	    //Check if the header is followed by a data packet
	    int packet_size = 0;
	    if ((polling_buffer[4] & 0x80) > 3 == 1)
		packet_size = polling_buffer[2];

	    //Check if the buffer contains at least one full message
	    if (polling_buffer.size() < HEADER_SIZE + packet_size)
		break;

	    MSG msg(polling_buffer, HEADER_SIZE + packet_size);
	    //Erase the formed message from the buffer.
	    polling_buffer.erase(polling_buffer.begin(),
				 polling_buffer.begin() + HEADER_SIZE + packet_size);
	    processMessage(msg);
	    std::cout << "MSG" << std::endl;
	    polling_msgs.push_back(msg);
	}
    } 

    void KIM101::processMessage(MSG msg)
    {
	// std::unique_lock<std::mutex> lock(mx);
	// std::unique_lock<std::mutex> position_lock(mx_position);
	std::cout << msg  << std::endl;
	polling_msgs.push_back(msg);
	if (msg == PZMOT_GET_STATUSUPDATE) {
	    x1 = (msg[11] << 24) | (msg[10] << 16) | (msg[9] << 8) | msg[8];
	    y1 = (msg[25] << 24) | (msg[24] << 16) | (msg[23] << 8) | msg[22];
	    x2 = (msg[38] << 24) | (msg[38] << 16) | (msg[37] << 8) | msg[36];
	    y2 = (msg[53] << 24) | (msg[52] << 16) | (msg[51] << 8) | msg[50];
	    cv_position.notify_all();
	}
	else if (msg == PZMOT_MOVE_COMPLETED) {
	    cv_move.notify_all();
	}
	// else 
	//     std::cout <<  msg << std::endl;

    }

//Send HW_REQ_INFO
    FT_STATUS KIM101::hw_req_info()
    {
	ftStatus = write(HW_REQ_INFO);
	return ftStatus;
    }


//Send HW_START_UPDATEMSGS
    FT_STATUS KIM101::hw_start_updatemsgs()
    {
	ftStatus = write(HW_START_UPDATEMSGS);
	return ftStatus;
    }


//Send HW_STOP_UPDATEMSGS
    FT_STATUS KIM101::hw_stop_updatemsgs()
    {
	ftStatus = write(HW_STOP_UPDATEMSGS);
	return ftStatus;
    }

//Enable a channel
    FT_STATUS KIM101::enable_channel(uint8_t channel)
    {
	if (channel > 0x06) {
	    std::cout << "Channel not available, aborting" << std::endl;
	    return FT_INVALID_PARAMETER;
	}

	PZMOT_SET_PARAMS_CHANENABLEMODE[8] = channel;
	write(PZMOT_SET_PARAMS_CHANENABLEMODE);
	usleep(500);
	write(PZMOT_REQ_PARAMS_CHANENABLEMODE);
	return FT_OK;
    }

    FT_STATUS KIM101::pzmot_move_absolute(uint8_t channel, long long int pos)
    {
	if (channel == 1) {
	    if (x1 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 2) {
	    if (y1 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 3) {
	    if (x2 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 4) {
	    if (y2 == pos) {
		return FT_OK;
	    }
	}
	std::unique_lock<std::mutex> lock(mx);
	enable_channel(channel);
	BYTE byte1 = (pos & 0x000000FF);
	BYTE byte2 = (pos & 0xFFFFFFFF) >> 8;
	BYTE byte3 = (pos & 0x00FF0000) >> 16;
	BYTE byte4 = (pos & 0xFF000000) >> 24;

	PZMOT_MOVE_ABSOLUTE[6] = (0x1 << (channel - 0x1));
	PZMOT_MOVE_ABSOLUTE[8] = byte1;
	PZMOT_MOVE_ABSOLUTE[9] = byte2;
	PZMOT_MOVE_ABSOLUTE[10] = byte3;
	PZMOT_MOVE_ABSOLUTE[11] = byte4;
	ftStatus = write(PZMOT_MOVE_ABSOLUTE);

	cv_move.wait(lock);
	return ftStatus;
    }

    FT_STATUS KIM101::set_position_counter(uint8_t channel, long long int pos)
    {
	if (channel == 1) {
	    if (x1 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 2) {
	    if (y1 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 3) {
	    if (x2 == pos) {
		return FT_OK;
	    }
	}
	else if (channel == 4) {
	    if (y2 == pos) {
		return FT_OK;
	    }
	}

	BYTE byte1 = (pos & 0x000000FF);
	BYTE byte2 = (pos & 0xFFFFFFFF) >> 8;
	BYTE byte3 = (pos & 0x00FF0000) >> 16;
	BYTE byte4 = (pos & 0xFF000000) >> 24;

	PZMOT_GET_PARAMS_POSCOUNTS[8] = (0x1 << (channel - 0x1));
	PZMOT_GET_PARAMS_POSCOUNTS[10] = byte1;
	PZMOT_GET_PARAMS_POSCOUNTS[11] = byte2;
	PZMOT_GET_PARAMS_POSCOUNTS[12] = byte3;
	PZMOT_GET_PARAMS_POSCOUNTS[13] = byte4;
	ftStatus = write(PZMOT_GET_PARAMS_POSCOUNTS);

	return ftStatus;
    }


    void KIM101::get_positions(bool verbose)
    {
	std::unique_lock<std::mutex> lock(mx_position);
	cv_position.wait(lock);
	if(verbose)
	    std::cout << "X1: " << x1 << ", Y1: " <<  y1
		    << ", X2: " << x2 << ", Y2: " <<  y2 << std::endl;
    }

    FT_STATUS KIM101::zero_all()
    {
	FT_STATUS ftStatus;
	ftStatus = pzmot_move_absolute(1,0);
	ftStatus = pzmot_move_absolute(2,0);
	ftStatus = pzmot_move_absolute(3,0);
	ftStatus = pzmot_move_absolute(4,0);
	return ftStatus;
    }



//Polling function. 
    void KIM101::msg_polling(int *polling_active) 
    {
	int *active_ptr = (int *)polling_active;
	while(*active_ptr == 1) {
	    usleep(100000);
	    read();
	}
    }

} //namespace end
