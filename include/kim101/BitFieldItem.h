#ifndef BIT_FIELD_ITEM__
#define BIT_FIELD_ITEM__

#include <cstdint>

namespace bf {

  template <typename _T, int _O, int _S> // Type, offset and size
  struct BitFieldItem
  {
    _T value;

    static_assert( _O + _S <= (std::size_t) sizeof(_T) * 8, "Member exceeds bitfield boundaries" );
    static_assert( _S < (std::size_t) sizeof(_T) * 8, "Bitfield does not fit inside the type provided" );

    static const _T max = (_T(1) << _S) - 1;
    static const _T mask = max << _O;

    _T one() const { return _T(1) << _O; }

    operator _T() const { return (value >> _O) & max; }

    BitFieldItem& operator=(_T v)
    {
      value = (value & ~mask) | ((v & max) << _O);
      return *this;
    }

    BitFieldItem& operator+=(_T v)
    {
      value += (v & max) << _O;
      return *this;
    }

    BitFieldItem& operator-=(_T v)
    {
      value -= (v & max) << _O;
      return *this;
    }

    BitFieldItem& operator++() { return *this += 1; }
    BitFieldItem& operator++(int) { return *this += 1; }

    BitFieldItem& operator--() { return *this -= 1; }
    BitFieldItem& operator--(int) { return *this -= 1; }
  };

}

#endif // BIT_FIELD_ITEM__
