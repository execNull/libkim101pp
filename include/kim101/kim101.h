#ifndef KIM101_H__
#define KIM101_H__
#include <ftd2xx.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <thread>
#include <vector>
#include <array>
#include <boost/signals2.hpp>
#include <condition_variable>
#include <atomic>

#define HEADER_SIZE 6
#define VID 0x0403
#define PID 0xFAF0

#define LIBKIM101_VERSION_MAJOR 0
#define LIBKIM101L_VERSION_MINOR 1
#define LIBKIM101_VERSION ((LIBKIM101_VERSION_MAJOR << 16) | LIBKIM101_VERSION_MINOR)

namespace LIBKIM101
{
using namespace boost::signals2;

/*!
* @brief scan for connected devices
* This function searches for connected devices. 
* 
*/
    FT_STATUS device_scan(bool verbose = true, std::vector<unsigned int>* serialNumbers = nullptr);


/*!
* @brief forms the messages that will be sent to the device. 
* 
* The MSG class represents a message that can be sent to the device. Every
* message is saved in a vector of uint8_t. 
*/
class MSG
{
public:
    /*! 
     * Null constructor
     */
    MSG() {};

    /*! 
     * Construct an MSG from an initializer list. 
     */
    MSG(std::initializer_list<uint8_t> msg);

    /*! 
     * Some messages are very long without any predefined bytes apart from the 
     * header. This constructor can be used to initialize such an MSG by 
     * providing the initial data (e.g. the header) and the size of the MSG in 
     * bytes. 
     */
    MSG(std::vector<uint8_t> msg, const int msg_size);

    friend std::ostream& operator<<(std::ostream& out, const MSG& o);
    bool operator== (const MSG& other ) const;
    uint8_t operator[] (int i) const { return data[i]; };
    uint8_t& operator[] (int i) {
	if (i > data.size()) {
	    std::cerr << " MSG::operator[] out of bounds index requested, aborting" << std::endl;
	    return data[0];
	}
	return data[i];
    }; 
    uint size();
private:
    std::vector<uint8_t> data;
};


/*!
* @brief represents the device. 
* 
* This class represents a KIM101 device. One instantiation will be needed for 
* each active device. 
* 
*/
class KIM101
{

public:
    KIM101() {};
    KIM101(const unsigned int SN);

    /*! 
     * Initialize the device. If only one device is found, no serial number is needed. 
     * If multiple devices are plugged in, the serial number can be passed as an argument. 
     */
    FT_STATUS init();
    FT_STATUS finalize();
    FT_STATUS hw_req_info();
    FT_STATUS hw_start_updatemsgs();
    FT_STATUS hw_stop_updatemsgs();
    FT_STATUS enable_channel(uint8_t ch);
    FT_STATUS pzmot_move_absolute(uint8_t channel, long long int pos);
    FT_STATUS set_position_counter(uint8_t channel, long long int pos = 0);
    FT_STATUS zero_all();
    void get_positions(bool vergose = true);
private:
    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    EVENT_HANDLE eh;
    DWORD EventMask;
    std::string serialNumber;

    FT_STATUS setPIDVID();
    FT_STATUS write(MSG msg);
    FT_STATUS read(int expectedBytes = 1);
    void processBytestream(BYTE buffer[], unsigned int nbytes);
    void processMessage(MSG msg);

    //Members related to the polling function
    int polling_active;
    void msg_polling(int *polling_active);
    std::vector<uint8_t> polling_buffer;
    std::vector<MSG> polling_msgs;
    std::thread polling;
    std::mutex mx;
    std::mutex mx_position;
    std::condition_variable cv_move;
    std::condition_variable cv_position;

    //KIM101 parameters
    long long int x1 = 0, y1 = 0, x2 = 0, y2 = 0;

    //Signals
    void hw_get_info_signal();
    signal<void()> hw_get_info_received();

    //Messages from the APT protocol
    MSG MOD_IDENTIFY = {0x23, 0x02, 0x00, 0x00, 0x50, 0x01};
    MSG HW_REQ_INFO = {0x05, 0x00, 0x00, 0x00, 0x50, 0x01};
    MSG HW_GET_INFO = {0x06, 0x00, 0x00, 0x00, 0xd0, 0x01};
    MSG HW_START_UPDATEMSGS = {0x11, 0x00, 0x00, 0x00, 0x50, 0x01};
    MSG HW_STOP_UPDATEMSGS = {0x12, 0x00, 0x00, 0x00, 0x50, 0x01};
    MSG PZMOT_SET_PARAMS_CHANENABLEMODE = {0xC0, 0x08, 0x04, 0x00, 0xD0, 0x01,
					    0x2B, 0x00, 0x00, 0x00};
    MSG PZMOT_REQ_PARAMS_CHANENABLEMODE = {0xC1, 0x08, 0x04, 0x00, 0xD0, 0x01,
					    0x2B, 0x00, 0x00, 0x00};
    MSG PZMOT_GET_PARAMS_CHANENABLEMODE = {0xC2, 0x08, 0x04, 0x00, 0xD0, 0x01,
					    0x2B, 0x00, 0x00, 0x00};
    MSG PZMOT_MOVE_ABSOLUTE = {0xD4, 0x08, 0x06, 0x00, 0xD0, 0x01,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    MSG PZMOT_MOVE_COMPLETED = {0xD6, 0x08, 0x0E, 0x00, 0x00, 0x00};
    MSG PZMOT_GET_STATUSUPDATE = {0xE1, 0x08, 0x38, 0x00, 0x81, 0x50};
    MSG PZMOT_GET_PARAMS_POSCOUNTS = {0xC0, 0x08, 0x0C, 0x00, 0xD0, 0x01,
				      0x05, 0x00, 0x00, 0x00, 0x00, 0x00,
				      0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
				      
};
}
#endif //KIM101_H__
