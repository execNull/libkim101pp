cmake_minimum_required(VERSION 3.1...3.15)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
    cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project (KIM101 VERSION 0.1
		   DESCRIPTION "Library for using KIM101 by Thorlabs" 
                   LANGUAGES CXX)

#set the 
set(HEADER_LIST "${KIM101_SOURCE_DIR}/include/kim101/kim101.h")
add_library(kim101 src/kim101.cxx ${HEADER_LIST})

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads)
include(cmake/FindFTD2XX.cmake)

find_package(Boost 1.53 REQUIRED COMPONENTS filesystem)
message(STATUS "Boost version: ${Boost_VERSION}")

target_include_directories(kim101 PUBLIC include ftd2xx Threads::Threads)

add_executable(move_channel apps/move_channel.cxx)
target_link_libraries(move_channel PUBLIC kim101 ftd2xx Threads::Threads)

add_executable(zero_all_channels apps/zero_all_channels.cxx)
target_link_libraries(zero_all_channels PUBLIC kim101 ftd2xx Threads::Threads)

add_executable(device_scan apps/device_scan.cxx)
target_link_libraries(device_scan PUBLIC kim101 ftd2xx Threads::Threads)

add_executable(get_positions apps/get_positions.cxx)
target_link_libraries(get_positions PUBLIC kim101 ftd2xx Threads::Threads)

add_executable(set_position_as apps/set_position_as.cxx)
target_link_libraries(set_position_as PUBLIC kim101 ftd2xx Threads::Threads)
