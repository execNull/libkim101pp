#include <kim101/kim101.h>

using namespace LIBKIM101;

int main(int argc, char* argv[])
{
    FT_STATUS status;
    std::vector<unsigned int> serialNumbers;
    device_scan(true, &serialNumbers);
    std::cout << "Number of devices found: " << serialNumbers.size() << std::endl;
    for (int i = 0; i < serialNumbers.size(); i++) {
	KIM101 kim(serialNumbers.at(i));

	status = kim.init();
	if (status != FT_OK) {
	    std::cout << "Failed to initialize device" << std::endl;
	    return status;
	}

	std::cout << "Device #" << serialNumbers.at(i) << ": ";
	kim.get_positions();

	status = kim.finalize();
    }
    return status;
}
