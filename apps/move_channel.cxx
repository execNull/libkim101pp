#include <kim101/kim101.h>

using namespace LIBKIM101;

int main(int argc, char* argv[])
{
    if (argc < 3) {
	std::cout << "expected arguments are channel and position" << std::endl;
	return 0;
    }
    FT_STATUS status;
    KIM101 kim;
    status = kim.init();

    int ch = atoi(argv[1]);
    long long int pos = atoi(argv[2]);

    status = kim.pzmot_move_absolute(ch, pos);
    kim.get_positions();

    status = kim.finalize();
    return status;
}
