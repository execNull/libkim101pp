#include <kim101/kim101.h>

using namespace LIBKIM101;

int main(int argc, char* argv[])
{
    FT_STATUS status;
    KIM101 kim;

    status = kim.init();

    kim.zero_all();

    status = kim.finalize();
    return status;
}
